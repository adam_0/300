% Secure Boot: Saving The Users From Themselves? - Quick Draft
% By Adam Hintz
% CSC 300: Professional Responsibilities
% Dr. Clark Turner

% Formatting {{{1

% One Column Format
\documentclass[11pt]{article}

\usepackage{hyperref}
\usepackage{url}

\usepackage[toc]{glossaries}
\usepackage{multicol}
% \usepackage{setspace} % I'm not sure if I'm using this one yet

% Page widening
\usepackage{geometry}
\geometry{letterpaper}

\loadglsentries{glossary}
\renewcommand{\glsdisplayfirst}[4]{\textbf{#1#4}}
\makeglossaries

% end Formatting }}}1

\begin{document} % {{{1

% Title Page {{{2
\title{\vfill Secure Boot: Saving The Users From Themselves?}
\author{\\
     Quick Draft\\
     By Adam Hintz\\
     CSC 300: Professional Responsibilities\\
     Dr. Clark Turner\\
}

\date{Oct 21, 2012}

\maketitle

\vfill

\begin{abstract} % {{{3

    Windows~8 is slated to be released on October 26,\cite{win-8-delivery-date}
    but there are already grave concerns that Windows 8 may be unnecessarily
    restricting hardware under the guise of a vulnerability
    fix.\cite{htg-explains-secure-boot} On ARM processors, Windows 8 will be the
    only operating system you can boot, with a new technology called Secure
    Boot.\cite{htg-explains-secure-boot}

    Is it ethical for Microsoft to restrict hardware that the user has
    purchased? Or should the user be the ultimate authority in all things,
    potentially leaving their system more vulnerable as a result? I believe that
    Microsoft is not acting ethically and should address users' concerns on the
    matter.

\end{abstract} % }}}3

% This page is still page #1, but the number won't show, as it is a cover page
\thispagestyle{empty}

% end Title Page }}}2

\newpage

% Table of Contents {{{2
\thispagestyle{empty}
\tableofcontents
% end Table of Contents }}}2

\newpage

\begin{multicols}{2}
\pagenumbering{arabic}

\section{Facts} % {{{2

\subsection{Rootkits are a serious problem} % {{{3

Microsoft's ``Malware Protection Center'' wrote an article about rootkit
malware.\cite{microsoft-on-rootkits} While they recommend using 64-bit Windows
due to a smaller percentage of rootkit attacks on such
environments,\cite{microsoft-on-rootkits} all machines are vulnerable to these
attacks. Rootkit attacks are often harder to eradicate than other
malware.\cite{eweek-rootkit-trojan} On top of that, rootkit sophistication is
growing.\cite{mcafee-rootkit}

% end Rootkits are a serious problem }}}3

\subsection{UEFI \& Secure Boot: Eliminating Rootkits} % {{{3

Windows 8 is changing from the traditional BIOS to UEFI to secure bootloader
code on certified Windows 8 hardware.\cite{no-easy-linux} This means that only
certified bootloaders can run, theoretically eliminating the problem of
rootkits: secure booting can provide ``the confidence that a rootkit will not be
able to substantively change the kernel of the operating
system.''\cite{readwriteweb-top-win-8-features} This is true for both the ARM
and x86 family of processors.\cite{microsoft-win-8-sys-req}

% end Windows 8 is using UEFI... }}}3

\subsection{ARM-based computers will have a different OS} % {{{3

Formerly called Windows on \gls{ARM}, \gls{Windows RT} is a version of Windows
optimized for ARM processors. It will include ``touch optimized desktop versions
of the new [MS Office Suite].''\cite{announce-win-rt}

% end ARM-based computers will have a different OS }}}3

\subsection{ARM is locked down on Windows 8} % {{{3

Secure boot ``must ship enabled'' on Windows~8 devices, though it can be
disabled on \gls{x86}.\cite{microsoft-win-8-sys-req} Microsoft has made it clear that
boot services will be locked down on Windows~8-certified ARM devices:

\begin{quotation}
    On non-ARM systems, it is required to implement the ability to disable
    Secure Boot via firmware setup. [But] programmatic disabling of Secure
    Boot either during Boot Services or after exiting \gls{EFI} Boot Services MUST NOT
    be possible. Disabling Secure Boot must not be possible on ARM systems.
    \cite{microsoft-win-8-sys-req}
\end{quotation}

% end sub ARM is locked down... }}}3

% end Known Facts }}}2

\section{Research Question} % {{{2

Is it ethical for Microsoft to use firmware-level protection to prevent third
party code from running at boot time?

This question is important for anyone buying a Windows~8 computer, especially
ARM-based computers. Their computers may be more secure, but they are forced to
only use the Windows~8 operating system.\cite{htg-explains-secure-boot}
In terms of acting ethically, Microsoft is caught between two issues: should
they protect their users from some forms of malware at the cost of limiting
hardware, or should the user be given freedom to use their hardware in their own
way, at the cost of increased security risks?

% end Research Question }}}2

\section{Others' Arguments} % {{{2

\subsection{Against - Microsoft is not acting ethically} % {{{3

\subsubsection{Free Software Foundation} %{{{4

Concerned groups on one side of the argument, such as the Free Software
Foundation, are worried that Microsoft will lock down the Windows platform and
prevent users from installing third-party operating systems, even perhaps on
other processor types in the future.\cite{fsf-concerns}

% end Free Software Foundation }}}4

\subsubsection{Dismissal of Secure Boot} % {{{4

Some public figures are dismissive of the implications of secure boot, one
expert going so far as to say ``Secure Boot will be broken.''
\cite{wired-secure-boot-linux} Linus Torvalds, the creator of Linux, believes
that ``clever hackers will bypass the whole key issue.''
\cite{wired-secure-boot-linux}

% end Dismissal of Secure Boot }}}4

% end Against }}}3

\subsection{For - Microsoft is acting ethically} % {{{3

\subsubsection{The ``Secure'' in Secure Boot} % {{{4

On the other side of the argument, some praise secure boot for the overall
security enhancements it will bring for users who are not concerned about
booting into other operating systems. They also note that Linux distributions
can buy keys for their own secure boot on the \gls{x86} family of processors, a
step that Ubuntu and Fedora are already prepared to
take.\cite{htg-explains-secure-boot}

% end The ``Secure'' in Secure Boot }}}4

\subsubsection{Microsoft's Explanation} %{{{4

Microsoft argues that it is taking ``no compromises on security… [ensuring]
that secured boot delivers a great security experience for our
customers.''\cite{ms-explain-secure-boot}

% end Microsoft's Explanation }}}4

%end For }}}3

%end Others' Arguments }}}2

\section{Analysis} % {{{2

\subsection{Introduction} % {{{3

Microsoft has always enjoyed a large majority of the market share in personal
computing OSes, accounting for well over 80\% of web traffic in the past 12
months.\cite{os-traffic-share} This is one reason why developments in new
versions of Windows are heavily scrutinized, and controversial decisions can
quickly become marketing nightmares.\cite{win-8-vs-rt}

\subsubsection{What is Secure Boot?} % {{{4

Microsoft gives a great explanation of what secure boot is:

\begin{quotation}
    UEFI has a firmware validation process, called secure boot, which is defined
    in Chapter 27 of the UEFI 2.3.1 specification. Secure boot defines how
    platform firmware manages security certificates, validation of firmware, and
    a definition of the interface (protocol) between firmware and the operating
    system.
\end{quotation}

Secure boot prevents malware from changing how your computer runs at startup by
cryptographically checking the operating system to make sure that it has not
been compromised. This also means that alternative operating systems cannot be
installed on a system which has secure boot enabled; the validation would fail,
as the third party OS would certainly differ from the originally installed one.

% end What is secure boot? }}}4

\textit{This section is sparse due to a need for more depth in the application
to the code of ethics. More will be added to the introduction to fill it in for
the final paper.}

% end Introduction }}}3

\subsection{Counter-Points} % {{{3

\textit{Again, stuff will be put here later, I was just shooting for a general
outline}

% end Counter-Points }}}3

\subsection{The Software Engineering Code of Ethics} % {{{3

If the Software Engineering Code of Ethics applies to Microsoft, then Microsoft
must (ethically) hold themselves to its standard. Since the code applies to "the
behavior and decisions made by professional software engineers, including
practitioners… managers, supervisors and policy
makers,"\cite[preamble]{se-code} which are just some of the positions held by
Microsoft employees. So Microsoft (and in particular, those parties responsible
for the creation and development of Windows 8) can be held to the SE Code of
Ethics.\cite{se-code}

% SE Codes in this paper: 1.05, 3.01, 8.02, 1.03 as backup

\subsubsection{SE Code Section 1.05 - Matters of Grave Public Concern} % {{{3

The Software Engineering Code of Ethics section 1.05 states that Software
Engineers shall ``cooperate in efforts to address matters of grave public
concern caused by software, its installation, maintenance, support or
documentation.''\cite[\S1.05]{se-code} 

% end SE-Code 1.05 }}}3

\subsubsection{Application of Section 1.05} % {{{3

Section 1.05 deals with ``matters of grave public concern,'' and some developers
have been quite concerned over secure boot.  There was great concern raised by
different members of the programming community when Microsoft announced that
secure boot would be a feature of Windows 8, especially by Red Hat developer
Matthew Garrett.\cite{mg-uefi-intro} Microsoft's
response\cite{ms-explain-secure-boot} did not satisfy Garrett, who stated that
the rebuttal was ``entirely factually accurate. But it's also
misleading.''\cite{mg-uefi-intro-2}

% end Application 1.05 }}}3

% end Analysis }}}2

% Finalize {{{2

\printglossaries

\end{multicols}

\newpage

\bibliographystyle{IEEEannot}
\bibliography{quick-draft}

% end Finalize }}}2

\end{document} % }}}1

% ellipsis characters: …
% vim: ft=tex tw=80 fdm=marker fdc=4 spell
