#!/bin/bash

FILE="quick-draft.tex"

if grep -q 'basically' $FILE; then
    echo "You said basically. No bueno."
    return -1
fi

if grep -q '"' $FILE; then
    echo "You have double quotes (\"). No bueno."
    return -2
fi

