#!/bin/bash

FILE="termpaper.tex"

echo "Uses of 'TODO':"
echo -n "termpaper.tex:"
grep "TODO" termpaper.tex | wc -l
echo -n "termpaper.bib:"
grep "TODO" termpaper.bib | wc -l
echo -n "glossary.tex: "
grep "TODO" glossary.tex | wc -l
echo
echo -n "Total:        "
grep TODO termpaper.tex termpaper.bib glossary.tex | wc -l

if grep -q 'basically' $FILE; then
    echo "You said basically. No bueno."
    return -1
fi

if grep -q '"' $FILE; then
    echo "You have double quotes (\"). No bueno."
    return -2
fi

