% Secure Boot: Saving The Users From Themselves? --- Final Draft
% By Adam Hintz
% CSC 300: Professional Responsibilities
% Dr. Clark Turner

% Formatting {{{1

% One Column Format
\documentclass[11pt]{article}

\usepackage{hyperref}
\usepackage{url}

\usepackage[toc, nonumberlist]{glossaries}
\usepackage{multicol}

% Page widening
\usepackage{geometry}
\geometry{letterpaper}

\loadglsentries{glossary}
\renewcommand{\glsdisplayfirst}[4]{\textbf{#1#4}}
\makeglossaries

% end Formatting }}}1

\begin{document} % {{{1

% Title Page {{{2
\title{\vfill Secure Boot: Saving The Users From Themselves?}
\author{\\
     By Adam Hintz\\
     CSC 300: Professional Responsibilities\\
     Dr. Clark Turner\\
}

\date{\today}

\maketitle

\vfill

\begin{abstract} % {{{3

    Windows~8 was recently released on October 26,\cite{win-8-delivery-date} but
    there are already grave concerns that Windows~8 may be unnecessarily
    restricting hardware under the guise of a vulnerability
    fix.\cite{htg-explains-secure-boot} On ARM processors, Windows~8 will be the
    only operating system you can boot with a new technology called
    secure boot.\cite{htg-explains-secure-boot}

    Is it ethical for Microsoft to restrict hardware that the user has
    purchased? Or should the user be the ultimate authority in all things,
    potentially leaving their system more vulnerable as a result? Microsoft is
    not acting ethically --- although they appear to be acting ethically by
    enhancing security, they should \emph{address users' concerns} on the matter
    by justifying their reasons for using secure boot, according to the Software
    Engineering Code of Ethics.\cite[\S1.05, \S3.01, \S3.08]{se-code}

\end{abstract} % }}}3

\thispagestyle{empty}

\newpage

% end Title Page }}}2

% Table of Contents {{{2

\thispagestyle{empty}

\tableofcontents

\thispagestyle{empty}

\newpage

\begin{multicols}{2}

\pagenumbering{arabic}

% end Table of Contents }}}2

\section{Facts} % {{{2

\subsection{Rootkits are a serious problem} % {{{3

Microsoft's ``Malware Protection Center'' wrote an article about rootkit
malware.\cite{microsoft-on-rootkits} In the article, they recommend using 64-bit
Windows due to a smaller percentage of rootkit attacks on such
environments;\cite{microsoft-on-rootkits} however all machines are vulnerable to
these attacks. Rootkit attacks can be harder to eradicate than other malware,
due to how they infect host systems.\cite{eweek-rootkit-trojan} On top of that,
rootkit sophistication has steadily been growing:

\begin{quote}
    Rootkits are a pervasive and evasive threat to today's systems. Increasingly
    sophisticated stealth techniques make detecting rootkits and stopping the
    damage they cause a significant challenge.\cite{mcafee-rootkit}
\end{quote}

% end Rootkits are a serious problem }}}3

\subsection{UEFI \& Secure Boot: Eliminating Rootkits} % {{{3

Windows 8 is changing from the traditional BIOS to UEFI in order to secure
bootloader code on certified Windows 8 hardware.\cite{no-easy-linux} This means
that only certified bootloaders can run, theoretically eliminating the problem
of rootkits: secure booting can provide ``the confidence that a rootkit will not
be able to substantively change the kernel of the operating
system.''\cite{readwriteweb-top-win-8-features} This is true for both the ARM
and x86 families of processors.\cite{microsoft-win-8-sys-req}

% end Windows 8 is using UEFI... }}}3

\subsection{Over 4 Million Windows 8 Upgrades Sold} % {{{3

In just the first three days since release, Microsoft has already sold over four
million copies of Windows 8 upgrades.\cite{ars-4-mil}

% end Over 4 Million copies }}}3

\subsection{ARM-based computers will have a different OS} % {{{3

Formerly called \emph{Windows on ARM}, Windows RT is a distinct and different
version of Windows which has been optimized for use on ARM processors. It will
include ``touch optimized desktop versions of the new [Microsoft Office
Suite].''\cite{announce-win-rt}

% end ARM-based computers will have a different OS }}}3

\subsection{ARM is locked down on Windows RT} % {{{3

Secure boot \emph{must ship enabled} on Windows~8 devices, though it can be
disabled on x86 processors.\cite{microsoft-win-8-sys-req} Microsoft has made it
clear that boot loaders will be locked down on Windows~8 certified ARM devices:

\begin{quotation}

    On non-ARM systems, it is required to implement the ability to disable
    Secure Boot via firmware setup. [But] programmatic disabling of Secure
    Boot either during Boot Services or after exiting EFI Boot Services
    MUST NOT be possible. Disabling Secure Boot must not be possible on ARM
    systems. \cite{microsoft-win-8-sys-req}

\end{quotation}

% end sub ARM is locked down... }}}3

% end Known Facts }}}2

\section{Research Question} % {{{2

Is it ethical for Microsoft to use firmware-level protection (such as UEFI's
secure boot) to prevent legitimate and illegitimate third party code from
running at boot time?

This question is important for anyone buying a Windows~8 computer, especially
ARM-based computers. Their computers may be more secure, but they are forced to
only use the Windows~8 operating system.\cite{htg-explains-secure-boot}
In terms of acting ethically, Microsoft is caught between two issues: should
they protect their users from some forms of malware at the cost of limiting
hardware, or should the user be given freedom to use their hardware in their own
way, at the cost of increased security risks?

% end Research Question }}}2

\section{Others' Arguments} % {{{2

\subsection{For --- Microsoft is acting ethically in its use of secure boot} % {{{3

\subsubsection{The ``Secure'' in Secure Boot} % {{{4

On one side of the argument, secure boot is being praised for the overall
security enhancements it is bringing for users who are not concerned about
booting into other operating systems.

Microsoft argues that it is taking ``no compromises on security\ldots [ensuring]
that secured boot delivers a great security experience for our
customers.''\cite{ms-explain-secure-boot}

% end The Secure in Secure Boot }}}4

\subsubsection{Computers Can Include Multiple Keys} %{{{4

With UEFI's secure boot, vendors can place multiple validation keys on a single
product. Linux distributions can buy keys for use with secure boot or even pay
Microsoft to be sign the Linux bootloader (Ubuntu and Fedora are already taking
similar steps).\cite{htg-explains-secure-boot} However, in order for keys to be
usable on any computer, the vendors must add these keys themselves during the
manufacturing process, as the users have no control about what keys are on their
machine (nor can keys be added or removed by the user).\cite{mg-uefi-intro-2}

% End Computers Can Include Multiple Keys }}}4

% end For }}}3

% begin Against {{{3

\subsection{Against --- Microsoft is not acting ethically in its use of secure
boot}

\subsubsection{Free Software Foundation} %{{{4

Concerned groups on the other side of the argument, such as the Free Software
Foundation, are worried that Microsoft is locking down the Windows platform in
Windows 8, with steps like secure boot and a closed app market. Secure boot
prevents users from installing third-party operating systems, removing control
from the hands of the users.\cite{fsf-concerns}

% end Free Software Foundation }}}4

\subsubsection{Dismissal of Secure Boot} % {{{4

Some public figures are dismissive of the implications of secure boot, one
expert going so far as to say ``secure boot will be broken.'' Linus Torvalds
(the creator of Linux) believes that ``clever hackers will bypass the whole key
issue.'' Prominent hacker George Hotz stated, ``I think Windows 8 is a last
ditch attempt by Microsoft to try to cling to ownership of the dominant API that
they lost when the Web rose to prominence.''\footnote{General citation for this
paragraph: \cite{wired-secure-boot-linux}}

% end Dismissal of Secure Boot }}}4

% end Against }}}3

\subsection{Summary of Arguments} % {{{3

Free software proponents argue that Secure Boot is a hostile act to prevent
users from installing other operating systems. If true, this would be a huge
blow to the Linux community and against hardware owners'
rights.\cite{fsf-concerns}

Microsoft points out that secure boot enhances the security in Windows 8
eliminating many forms of rootkit attacks,\cite{ms-explain-secure-boot} which
can be very difficult to eradicate.\cite{mcafee-rootkit} Vendors can also get
their own keys or have Microsoft sign their operating
system,\cite{htg-explains-secure-boot} although this is not an easy task and
does not allow for developers to build their own bootloaders and kernels on such
machines.\cite{mg-uefi-intro}

% end Summary }}}3

%end Others' Arguments }}}2

\section{Analysis} % {{{2

\subsection{The Software Engineering Code of Ethics --- Overview} % {{{3

If the Software Engineering Code of Ethics applies to Microsoft then Microsoft
must be held to its standard ethically. Microsoft directly states that it
employs software engineers to work on Windows software on their website's
careers page.\cite{microsoft-hiring} Therefore the Software Engineering Code of
Ethics applies to ``\textbf{the behavior and decisions made by professional
software engineers, including practitioners\ldots managers, supervisors and
policy makers}'' at Microsoft.\cite[Preamble]{se-code} (Microsoft also aligns
itself with similar values to the Software Engineering Code in its mission
statement and under the business information overview on their
website\cite{microsoft-mission, microsoft-business})

Because the Software Engineering Code applies to Microsoft engineers, they
``must commit themselves to making software engineering a beneficial and
respected profession\ldots and shall adhere to the Code of Ethics and
Professional Practice.''\cite[Preamble]{se-code}

% end The Software Engineering Code of Ethics --- Overview }}}3

\subsection{Introduction} % {{{3

Microsoft has always enjoyed a large majority of the market share in personal
computer operating systems, accounting for well over 80\% of all web traffic
(including mobile traffic) in the past 12 months.\cite{os-traffic-share} This is
one reason why developments in new versions of Windows are heavily scrutinized,
and controversial decisions can quickly become marketing
nightmares.\cite{win-8-vs-rt} Because of their large influence, Microsoft must
be very careful to ensure that they are acting ethically and
responsibly.\cite{se-code}

\subsubsection{What is Secure Boot?} % {{{4

Microsoft gives a great explanation of what secure boot is:

\begin{quotation}
    UEFI has a firmware validation process, called secure boot, which\ldots
    defines how platform firmware manages security certificates, validation of
    firmware, and a definition of the interface (protocol) between firmware and
    the operating system.\cite{ms-explain-secure-boot}
\end{quotation}

Secure boot prevents rootkit malware from changing how your computer runs at
startup by checking the operating system's boot loader to make sure that it has
not been compromised. This also means that alternative operating systems cannot
be installed on a system which has secure boot enabled; the validation would
fail, as the third-party operating system's boot loader would differ from
Microsoft's.\cite{htg-explains-secure-boot} So not only does secure boot prevent
rootkit malware from running on your computer, but it also prevents
user-installed, third-party operating systems from being run.

% end What is secure boot? }}}4

\subsubsection{Some Definitions} % {{{4

Throughout this paper, references are made to two groups of users: those who
wish to boot into multiple operating systems (\emph{multi-booting users}) and
those who do not need to boot into multiple operating systems (\emph{average
users}). For example, \emph{average users} might not even know that secure boot
is in effect on their Windows 8 or Windows RT computer, but it actively provides
additional security for them.\cite{ms-explain-secure-boot} An example of a
\emph{multi-booting user} is a Linux user who wishes to buy a laptop and install
and use Linux on it instead of the factory-default Windows.

The reason these two groups are differentiated is that these groups have
conflicting requirements on how Microsoft should act. When analyzing whether or
not Microsoft is acting ethically, these two groups are at odds.

For the average user, secure boot is in the implicit
requirements\footnote{`Implicit requirements' is a term in software engineering
    referring to a user's requirement that can be implied from general
    knowledge. In this case, average users implicitly require a more secure
operating system --- they might not be asking for it, but it is still critically
important.\cite{microsoft-implicit}}, but for a multi-booting user, secure boot
opposes their requirements.

% end Some Definitions }}}4

% end Introduction }}}3

% SE Codes in this paper: 1.05, 3.01, 3.08

\subsection{Considerations only with Ethical Systems} % {{{3

\subsubsection{Rawls' Second Principle of Justice} % {{{4

Microsoft is acting ethically under John Rawls' Justice Principles, specifically
the Second Principle of Justice. This states that Microsoft's products should be
of the greatest benefit of the least-advantage members of
society.\cite{rawlsian} In a manner of speaking, the \emph{least-advantaged}
computer users are those who do not understand the inner workings of a computer
(these \emph{average users} won't need nor want to install any additional
operating systems). With secure boot in Windows 8, these average users receive
the benefit of a more secure operating system.\cite{ms-explain-secure-boot} On
the other side, \emph{multi-boot users} are developers and technically savvy
computer users who are more advantaged and knowledgeable in the area of
operating computers. Using secure boot in Windows 8 benefits the least
advantaged: the \emph{average user}. Taking John Rawls' ethics principles into
account with the issue of secure boot (specifically the second Principle of
Justice), Microsoft is acting ethically.\cite{rawlsian}

% end Rawls' Second Principle of Justice }}}4

% Other Considerations with Ethical Systems }}}3

\subsection{SE Code Section 3.08 --- Satisfying Users' Requirements} % {{{3

Section 3.08 of the SE Code of Ethics states that software engineers must
``\textbf{ensure that specifications for software on which they work\ldots
satisfy the users' requirements.}''\cite[\S3.08]{se-code}

\subsubsection{Definitions and Clarification for Section 3.08} % {{{4

Many \emph{specification} details on how secure boot is to be used in Windows 8
are detailed in the Windows 8 system requirements
document.\cite{microsoft-win-8-sys-req} The question, then, is if this
specification satisfies the users' requirements.

Who are the `users' that the code refers to? Anyone who uses Windows 8 is a
`user,' but as mentioned previously, there are two main groups with conflicting
requirements: the average users and the multi-booting users. How Microsoft
should resolve this issue will be discussed later.

According to Merriam-Webster, a `requirement' is ``something essential to the
existence or occurrence of something else.''\cite{mw-requirement} In the case of
software, requirements are features that the users state are \emph{essential} to
making the software useful. Software that does not satisfy user requirements is
less useful --- and according to Section 3.08, it is unethical.

% end Definitions and Clarification }}}4

\subsubsection{Restating Section 3.08} % {{{4

To restate the Software Engineering Code Section 3.08 in the context of this
issue:

\begin{quote}

    Microsoft should ensure that specifications for Windows 8 and Windows RT
    satisfy users' requirements.

\end{quote}

% end Restating Section 3.08 }}}4

\subsubsection{Application of Section 3.08 With Ethics Systems} % {{{4

When considering whether or not Microsoft is satisfying users' requirements,
there is a conflict. As stated earlier in the paper there are two groups of
users, and their requirements differ when it comes to secure boot. The
\emph{average users} are in favor of secure boot and the security enhancements
that it gives, but the \emph{multi-boot users'} requirements are at odds with
secure boot because it limits what they can do with computer hardware they have
purchased. This is the key to deciding whether or not Microsoft is acting
ethically.

On one hand, Norton estimates that viruses cost around \textbf{\$388 billion}
annually to ``global cybercrime'' and ``time lost'' due to computer
infection.\cite{symantec-cost} On the other hand, website LinuxCounter.net
estimates a little less than 63 million Linux users at the time of
writing.\cite{linux-counter} Even assuming that in a Windows 8 era, all of these
users wish to buy a new computer and experience some sort of additional cost for
Linux compatibility, the costs don't match those of viruses. This opportunity
cost would have to equal \emph{\$6158.73} in additional cost per computer to
equal the damage of viruses every year.\cite{symantec-cost} With the average
price of notebooks in the US at around \$700, it's extremely unlikely that users
will experience a nearly 880\% price increase as a result of secure boot
alone.\cite{avg-laptop-price} Financially, more weight resides with reducing
cybercrime.

With utilitarianism, Microsoft is working towards the `greater good,' which
involves giving the greatest happiness to the most people, in line with the
``Greatest Happiness Principle'' that John Stuart Mill discusses in his book
``Utilitarianism.''\cite{utilitarianism} By increasing the security of computers
certified for use with Windows 8 (and thus reducing the cost, both financial and
time, of users dealing with viruses), Microsoft is increasing the utility and
happiness of users.\cite{utilitarianism} Thus according to utilitarian ethics
and the Software Engineering Code of Ethics section 3.08, Microsoft is acting
ethically by increasing the overall happiness (and thus utility) of a majority
of its users.

% end Application of Section 3.08 }}}4

% end SE-Code 3.08 }}}3

\subsection{SE Code Section 1.05 --- Matters of Grave Public Concern} % {{{3

The Software Engineering Code of Ethics section 1.05 states that Software
Engineers shall ``\textbf{cooperate in efforts to address matters of grave
public concern caused by software, its installation, maintenance, support or
documentation.}''\cite[\S1.05]{se-code}

\subsubsection{Definitions and Clarification for Section 1.05} % {{{4

According to Merriam-Webster, to `cooperate' is ``\emph{to act or work with
another or others\ldots to associate with another or others for mutual
benefit.}''\cite{mw-cooperate} In order for Microsoft to act ethically on the
matter of secure boot they need to work with the public to address any grave
concerns they might have related to Windows 8.

`Grave' may seem like too strong of a phrase to use when talking about the
implications of secure boot on Windows RT for ARM devices, but Merriam-Webster
defines the adjective form of the word `grave' as ``\textbf{meriting serious
consideration\ldots significantly serious.}''\cite{mw-grave}

Merriam-Webster says that to `concern' is ``to be a care, trouble, or distress
to.''\cite{mw-concern} A search query for ``Microsoft secure boot arm'' on
Google yields over 10 million results.\cite{google-ms-secure-boot-arm}
Therefore, the public believes this is a serious concern which merits
consideration from Microsoft; \textbf{the implications of secure boot on ARM
devices are of `grave public concern.'}

% end Definitions and Clarifications }}}4

\subsubsection{Restating Section 1.05} % {{{4

To restate the Software Engineering Code Section 1.05 in the context of this
issue:

\begin{quote}

    Microsoft should work with \emph{multi-boot users} in efforts to address the
    issue of being unable to install a third-party operating system because of
    secure boot with Windows RT on ARM devices.

\end{quote}

% end Restating }}}4

\subsubsection{Application of Section 1.05} % {{{4

Along the line of `grave public concern', developers and technical blogs
continue to express concerns over several issues surrounding the latest release
of Windows.

In the days before release, even Microsoft store employees were confused about
what the differences between Windows 8 and Windows RT were. Some employees were
even stating false information about the capabilities of the two similar
operating systems.\cite{win-8-vs-rt} Because Windows RT only runs on ARM, an
entirely different architecture from x86, these misinformed employees also
couldn't explain to customers how secure boot could affect their purchase.

There was also great concern raised by several members of the programming
community when Microsoft announced that secure boot would be a feature of
Windows 8, especially by Red Hat developer Matthew Garrett.\cite{mg-uefi-intro,
mg-uefi-intro-2, fsf-concern} Microsoft's response to Garrett's
concerns\cite{ms-explain-secure-boot} did not satisfy him. He stated that the
rebuttal was ``entirely factually accurate. But it's also
misleading.''\cite{mg-uefi-intro-2} Although Microsoft responded in a sense,
they did not fully `cooperate' with the public because they did not directly
respond to the concerns of \emph{multi-booting users}.

\emph{Cooperation} on Microsoft's end would at the very least entail a blog post
weighing pros and cons of secure boot, with an explanation of why Microsoft
chose secure boot over not having it or allowing it to be disabled. Instead,
``Microsoft haven't even attempted to argue [against the disadvantages of secure
boot].''\cite{mg-uefi-intro-2} Because Microsoft is not communicating with
concerned users and the public, they are not acting ethically according to the
Software Engineering Code of Ethics Section 1.05.\cite{se-code}

% end Application 1.05 }}}4

% end SE-Code 1.05 }}}3

\subsection{SE Code Section 3.01 --- Significant Tradeoffs} % {{{3

The Software Engineering Code of Ethics Section 3.01 states that Software
Engineers shall ``\textbf{strive for high quality\ldots ensuring significant
tradeoffs\ldots are available for consideration by the user and the
public.}''\cite[\S3.01]{se-code}

\subsubsection{Definitions and Clarifications for Section 3.01} % {{{4

Merriam-Webster defines `strive' as ``to devote serious effort or
energy,''\cite{mw-strive} which means that in the development of software,
Microsoft should devote serious effort to making its products as high-quality as
possible.

`Significant' is defined as ``having or likely to have influence or
effect.''\cite{mw-significant} A `tradeoff' is ``a giving up of one thing in
return for another.''\cite{mw-tradeoff} Stated more simply, Microsoft should
describe the significant tradeoffs in terms of pros and cons. For this issue,
secure boot is the `significant tradeoff' in question: with respect to secure
boot, should Microsoft increase security (pro) or allow alternate OSes to boot
(con)?

Having significant tradeoffs ``available for consideration'' is simple:
Microsoft already makes informational posts (about the goings-on at Microsoft)
to inform the public. They would simply need to make a blog post on their
website detailing the pros and cons of secure boot to conform with the
code.\cite{ms-explain-secure-boot}

% end Definitions and Clarifications }}}4

\subsubsection{Restating Section 3.01} % {{{4

To restate the Software Engineering Code Section 3.01 in the context of the
issue of secure boot on ARM:

\begin{quote}

    Microsoft should devote serious effort to high-quality products, ensuring
    that they explain the pros and cons of secure boot online for public
    consumption.

\end{quote}

% end Restating Section 3.01 }}}4

\subsubsection{Application of Section 3.01 With Ethics Systems} % {{{4

The latter part of the rephrasing is the pivotal part of the following analysis:
is Microsoft stating pros and cons online for the public to view and debate?  If
Microsoft's arguments (pro and con) regarding secure boot were available,
Microsoft would be acting ethically according to the Software Engineering Code
of Ethics Section 3.01. Through several searches of Microsoft's website with
terms like ``secure boot against''\cite{google-sb-against} or ``secure boot
con,''\cite{google-sb-con} I found very few results. Furthermore, these results
were all either user generated content featuring no official statement or an
official statement devoid of any evaluation of the problems that secure boot
could have for some users. Microsoft has not published any information to the
public detailing any perceived or actual drawbacks or cons to secure boot. All
of the information published by Microsoft talks about the positive security
enhancements to secure boot.\cite{ms-explain-secure-boot, ms-win-8-boot-exp,
ms-security}

According to Stanford's Encyclopedia of Philosophy, contractarian deontological
theories state that ``\emph{morally wrong acts are\ldots acts that would be
forbidden by principles that people in a suitably described \textbf{social
contract} would accept.}''\cite{deontology} Merriam-Webster defines a social
contract as ``an actual or hypothetical agreement among the members of an
organized society\ldots that defines and limits the rights and duties of
each.''\cite{mw-social-contract}

The Software Engineering Code of Ethics acts as a \emph{social contract} between
software engineers and the public, employers, colleagues, and others. The
preamble states that the code ``is a means to educate\ldots about the
\textbf{ethical obligations} of all software
engineers.''\cite[Preamble]{se-code}

To sum up: since the Code acts as a social contract for software engineers under
contractarian deontological ethics, software engineers (Microsoft) commit
morally wrong acts when doing anything forbidden by the Software Engineering
Code of Ethics or anything that goes against the Code.

Because Microsoft's actions conflict with the Software Engineering Code of
Ethics Section 3.01 (by not making tradeoffs available for consideration by the
public), Microsoft is acting \textbf{unethically} when viewed through the lens
of contractarian deontological theories.\cite{deontology, se-code}

% end Application of 3.01 }}}4

% end SE Code Section 3.01 }}}3

% SE Code Section 6.07 {{{3

\subsection{SE Code Section 6.07 --- Be Accurate\ldots Avoiding False Claims}

SE Code Principle 6 provides rules regarding the profession of Software
Engineering. Section 6.07 says that software engineers must:

\begin{quote}
     Be accurate in stating the characteristics of software on which they work,
     avoiding not only false claims but also claims that might reasonably be
     supposed to be speculative, vacuous, deceptive, misleading, or
     doubtful.\cite[\S6.07]{se-code}
\end{quote}

\subsubsection{Definitions and Clarification for Section 6.07} % {{{4

Merriam-Webster defines `accurate' as ``conforming exactly to truth or to a
standard; exact.''\cite{mw-accurate} `Deceptive' is unhelpfully defined as
``tending or having power to deceive,'' and to deceive is ``to give a false
impression.''\cite{mw-deceptive, mw-deceive}

% end Definitions and Clarification

\subsubsection{Restating Section 6.07} % {{{4

\begin{quote}

     Microsoft must be accurate in stating the characteristics of Windows 8's
     secure boot feature, avoiding not only false claims but also claims that
     might reasonably be supposed to give a false impression.

\end{quote}

% end Restating }}}4

\subsubsection{Application of Section 6.07 with Secure Boot} % {{{4

By posting only positive information about secure boot, Microsoft is giving a
false impression as to what secure boot does.\cite{ms-explain-secure-boot,
ms-win-8-boot-exp, ms-security} This false impression, while not directly giving
a false claim, is still in opposition to section 6.07 of the SE Code of Ethics.
Microsoft is acting unethically by only telling us part of the story when it
comes to how Windows 8's specifications are affecting the hardware that users
purchase.\cite{microsoft-win-8-sys-req}

% End Application }}}4

% end SE Code Principle 6 }}}3

\subsection{Conclusion} % {{{3

Microsoft is in a tricky situation: their users have differing and conflicting
requirements for them. While the \emph{average users} need more security and are
thus in favor of secure boot, the \emph{multi-boot users} need to be allowed to
modify the bootloader and are thus opposed to secure boot. In considering
whether or not Microsoft is acting ethically by utilizing secure boot in Windows
RT, an examination must be made of whether Microsoft should side with one group
or the other (using established ethical systems).

\subsubsection{Arguments that Microsoft \emph{is} Acting Ethically} % {{{4

In some ways, Microsoft \emph{is acting ethically} by including secure boot into
Windows 8. The following ethical points are in favor of Microsoft:

\begin{itemize}

    \item \textbf{Rawlsian Justice Principles} - John Rawls' Second Principle of
        Justice states that Microsoft should favor the ``least-advantaged''
        computer users the most: the \emph{average users}.\cite{rawlsian} In
        using secure boot in Windows 8, Microsoft is favoring these users, who
        are not as technically savvy as the \emph{multi-boot users}.

    \item \textbf{Section 3.08} - The Software Engineering Code of Ethics states
        that Microsoft must make sure that specifications ``satisfy the users'
        requirements.''\cite[\S3.08]{se-code} Because there are two conflicting
        sets of requirements (between the average users and the multi-boot
        users), utilitarianism can be used to show that Microsoft is acting
        ethically by trying to provide the greatest happiness for the larger
        group of people, the \emph{average users}.\cite{symantec-cost,
        linux-counter, utilitarianism}

\end{itemize}

% end Pro-Microsoft Arguments }}}4

\subsubsection{Arguments that Microsoft \emph{is not} Acting Ethically} % {{{4

The points at which Microsoft is no longer acting ethically are Software
Engineering Code of Ethics \textbf{Sections 1.05, 3.01, and 6.07}.

The SE Code section 1.05 states that Microsoft shall ``cooperate in efforts to
address matters of grave public concern'' that may be caused by Windows RT or
its features.\cite[\S1.05]{se-code} Secure boot has caused `grave public
concern,' and Microsoft has posted the positive effects of secure boot on
computer operation,\cite{ms-explain-secure-boot} but ``Microsoft haven't even
attempted to argue [against the disadvantages of secure
boot].''\cite{mg-uefi-intro-2} Microsoft should directly confront the
\emph{multi-boot users'} qualms about secure boot and explain why they chose to
add secure boot to Windows 8 despite these users' concerns. Microsoft could do
this easily, as it does many other public announcements, by posting this
information on its blog. However, since Microsoft does not address the cons of
secure boot, Microsoft is not acting ethically with respect to Software
Engineering Code of Ethics Section 1.05.

The SE Code section 3.01 states that Microsoft shall ensure that ``significant
tradeoffs\ldots are available for consideration by the user and the
public.''\cite[\S3.01]{se-code} Similarly to section 1.05, Microsoft
\emph{could} be ethical by taking the time to publicly explaining the pros and
cons of secure boot, but it is not doing so. Because of this, Microsoft is
acting against the Code. Contractarian deontological ethics explain that the SE
Code acts as a \textbf{social contract} between Microsoft and the users /
public. By not following this social contract, they are acting unethically under
the Software Engineering Code of Ethics Section 3.01, in conjunction with
contractarian deontological ethics.\cite{se-code, deontology}

The SE Code section 6.07 states that Microsoft must ``be accurate in stating the
characteristics'' of Windows 8.\cite[\S6.07]{se-code} By putting a positive
information on what little information they release, Microsoft is not acting
ethically according to section 6.07.\cite{ms-explain-secure-boot,
ms-win8-boot-exp, ms-security, se-code}

% end anti-Microsoft Arguments }}}4

\subsubsection{Closing}

Microsoft may be acting ethically by utilizing the security advantages of secure
boot in Windows 8, but by not rationalizing their decisions, they conflict not
only with the sections mentioned in this paper\footnote{\S1.05, 3.01, 3.08,
6.07}, but also in general with principle 6 of the Software Engineering Code of
Ethics.  Principle 6 involves promoting public knowledge of the profession and
products of software engineering; Microsoft does not accurately convey
information (both pros and cons) surrounding their use of secure boot in Windows
8.\footnote{General citation for this section: the Software Engineering Code of
Ethics\cite[\S1.05, 3.01, 3.08, 6.07, 6]{se-code}}

Though Microsoft has the correct intentions by using secure boot --- to improve
users' quality of life by attempting to eradicate rootkit malware --- they
\emph{must} convey this information to the public in order to be fully ethical.

Even though Microsoft's use of secure boot in Windows 8 is ethical (under
Section 3.08), the lack of official information weighing the pros and cons of
secure boot is \textbf{unethical} under Sections 1.05, 3.01, and 6.07 of the
Software Engineering Code of Ethics.

% end Conclusion }}}3

% end Analysis }}}2

% Finalize {{{2

% Glossary {{{3

\newpage

\glsaddall
\printglossaries

% end Glossary }}}3


\end{multicols}

\newpage

\phantomsection
\addcontentsline{toc}{section}{References}
\nocite{*}
\bibliographystyle{IEEEannot}
\bibliography{termpaper}

% end Finalize }}}2

\end{document} % }}}1

% ellipsis: \ldots
% em dash: --- (that's three dashes)
% bold text: \textbf{bolded text goes here}
% italics: \emph{italicized text goes here}

% vim: ft=tex tw=80 fdm=marker fdc=4 spell
