# vim: set ts=5 sw=5 noet nolist:
# Make File for Adam Hintz's Term Paper
# Written by Ross Light based on a Makefile by Nat Welch
# ...and then modified by Adam Hintz
#
# Targets:
#
#   run   --- make a pdf and then open it
#   pdf   --- builds the pdf version of the document
#   clean --- cleans up everything that can be regenerated

NAME=termpaper

TEXFILES=\
	$(NAME).tex\
	glossary.tex

BIBFILES=\
	$(NAME).bib

# Programs
PDFLATEX  = pdflatex
BIBTEX    = bibtex

# Targets
all: pdf

run: pdf
	@open -a "Preview" $(NAME).pdf

count: $(NAME).tex
	@./texcount.pl -v -html $(NAME).tex > /tmp/wc.html
	@open -a "google chrome" /tmp/wc.html

count-gloss: glossary.tex
	@./texcount.pl -v -html glossary.tex > /tmp/glossary-wc.html
	@open -a "google chrome" /tmp/glossary-wc.html

pdf: $(NAME).pdf

%.pdf: $(TEXFILES) $(BIBFILES)
	@echo "--- LaTeX"
	@$(PDFLATEX) $(NAME)
	@echo
	
	@echo "--- BibTeX"
	@$(BIBTEX) $(NAME)
	@echo
	
	@echo "--- Glossary"
	@makeglossaries $(NAME)
	@echo
	
	@echo "--- Running LaTeX until complete"
	@$(PDFLATEX) $(NAME)
	@$(PDFLATEX) $(NAME)
	@$(PDFLATEX) $(NAME)

clean:
	rm -f *.pdf *.aux *.bbl *.blg *.glo *.glg *.gls *.ist *.log *.out *.toc
